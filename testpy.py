import serial
import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="",
  database="special_io"
)

adrduinoData = serial.Serial("com3")

mycursor = mydb.cursor()


sql = "INSERT INTO datatest (x, y) VALUES (%s, %s)"
val = ("25", "89")

mycursor.execute(sql, val)

mydb.commit()

print(mycursor.rowcount, "record inserted.")