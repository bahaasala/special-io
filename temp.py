import time
import serial
import mysql.connector

ser = serial.Serial('COM3', 9600)  # open serial port

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="",
    database="special_io"
)
mycursor = mydb.cursor()

while True:
    value = ser.readline()
    value = str(value, 'utf-8').rstrip(":\n").split(":")
    print(value)
    time.sleep(60)

    sql = "INSERT INTO temperature (temp1, temp2) VALUES (%s ,%s)"
    val = (value[0], value[1])

    mycursor.execute(sql, val)

    mydb.commit()
