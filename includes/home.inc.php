<head>
    <meta http-equiv="refresh" content="300">
    <script type="text/javascript" src="js/refresh.js"></script>
</head>

<a href="./php/logout.php" class="logout-button">LOG OUT</a>

<?php
include ('./php/database-config.php');

$dbname = 'special-io';
try{
    $sql = "SELECT id, temp1, temp2, time
                FROM temperature ORDER BY id DESC LIMIT 30";

    $q = $conn->prepare($sql);
    $q->setFetchMode(PDO::FETCH_ASSOC);
    $q->execute();

} catch (PDOException $e) {
    die("Could not connect to the database $dbname :" . $e->getMessage());
}
?>

<head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            let data = google.visualization.arrayToDataTable([
                ['ID','Temp1', 'Temp2'],
                <?php
                $CelsiusChar = '°C';
                $unicodeChar = '&#8451;';

                foreach($conn->query($sql) as $row){
                    echo "[".$row["id"].", ".$row["temp1"].", ".$row["temp2"]."],";
                }

                ?>
            ]);

            let options = {
                title: 'The temperature in celsius',
                titleTextStyle: {
                    fontSize: 20,
                    bold: true
                },
                curveType: 'function',
                legend: { position: 'bottom' },
            };

            let chart = new google.visualization.LineChart(document.getElementById('temp_chart'));

            chart.draw(data, options);
        }
    </script>
</head>
<div id="temp_chart"></div>

    <table id="temperature">
        <tr>
            <th>Date and Time</th>
            <th>Temperature 1</th>
            <th>Temperature 2</th>
            <th>Measure</th>
        </tr>
                    <?php while ($row = $q->fetch()): ?>
                        <tr>
                            <td><?php echo htmlspecialchars($row['time']) ?></td>
                            <td><?php echo htmlspecialchars($row['temp1']), $unicodeChar ?></td>
                            <td><?php echo htmlspecialchars($row['temp2']), $unicodeChar ?></td>
                            <td><?php echo htmlspecialchars($row['id']); ?></td>
                        </tr>
                    <?php endwhile; ?>
<br>
</table>
<div class="footer"><h3>Made by: Bahaa Salaymeh</h3></div>


