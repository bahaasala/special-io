<div class="login-page">
    <div class="login-form">
        <form name="login" action="./php/login.php" method="Post">
            <h1 class="intro-text">Welcome to special i/o</h1><br />
            <div class="login-fields">
                <label>Username</label><br />
                <input class="username-field" type="text" name="username" required/><br />
                <label>Password</label><br />
                <input class="password-field" type="password" name="password" required/><br />
                <p>Don't have an account?</p>
                <p><a href="/index.php?page=register">Register now</a></p>
                <button class="login-button" type="submit" name="submit" value="Submit">LOG IN</button>
            </div>
        </form>
    </div>
</div>
