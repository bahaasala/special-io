<div class="login-page">
    <div class="register-form">
        <form name="register" action="./php/register.php" method="Post">
            <h1 class="intro-text">Welcome to special i/o</h1>
            <a href="/index.php?page=login" class="back-button">Back to LOGIN</a>
            <h2 class="intro-text">Create an account</h2>
            <div class="login-fields">
                <label>Create a username</label><br />
                <input class="username-field" type="text" name="username" required/><br />
                <label>Create a password</label><br />
                <input class="password-field" type="password" name="password" required/><br />
                <button class="register-button" type="submit" name="submit" value="Submit">SIGN UP</button>
            </div>
        </form>
    </div>
</div>