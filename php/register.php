<?php
include ('database-config.php');


if(isset($_POST['submit'])) {

    $username = $_POST['username'];
    $password = $_POST['password'];

    $stmt = $conn->prepare("SELECT username FROM users WHERE username = :username");
    $stmt->bindParam(':username', $username);
    $stmt->execute();

    if($stmt->rowCount() > 0){
        echo "exists!";
        header("Location: ../index.php?page=register");
    } else {
        $insert = $conn->prepare("INSERT INTO users (username,password) values(:username,:password);");
        $insert->bindParam(':username', $username, PDO::PARAM_STR);
        $insert->bindParam(':password', $password, PDO::PARAM_STR);
        $insert->execute();

        header("Location: ../index.php?page=login");
    }



}

?>