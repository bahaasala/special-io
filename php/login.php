<?php

require 'database-config.php';

session_start();

$username = "";
$password = "";

if (isset($_POST['username'])) {
    $username = $_POST['username'];
}
if (isset($_POST['password'])) {
    $password = $_POST['password'];
}


$sql = $conn->prepare("SELECT * FROM users WHERE username = :username AND password = :password");
$sql->execute(array(':username' => $username, ':password' => $password));


if ($sql->rowCount() == 0) {
    header('Location: ../index.php?page=login');
} else {
    $row = $sql->fetch(PDO::FETCH_ASSOC);
    header('Location: ../index.php?page=home');
}


